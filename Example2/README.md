# Demo to run spark ml model for inference on KML 

# Model details
1. The model training part is from train.rf3.py
2. The model take numerical input variables which are the result of a PCA transformation(v1, ... v28)
3. It output an integer 0/1 indicating fraud.

# How to
1. Build the docker image and push to docker hub or private docker registry
2. Modify model.json file if needed
3. Change the ip in registerkml.sh and run it `bash registerkml.sh`, this register the model. This step can also be done manually from UI(Models+Analytics-->Add Model-->New Blackbox)
4. Change url accordingly, Test with following:
   ```
   curl --location --request POST 'http://172.30.25.62:9187/kml/model/deployment/42/infer' \
    --header 'Content-Type: application/json' \
    --data-raw '[{
        "v1": -7.792,
        "v2": -10.7422,
        "v3": 1.7233,
        "v4": 0.3697,
        "v5": 8.3067,
        "v6": -5.7118,
        "v7": -7.227,
        "v8": 0.9843,
        "v9": 0.8826,
        "v10": 1.1957,
        "v11": 0.9241,
        "v12": 0.0691,
        "v13": -1.446,
        "v14": -0.125,
        "v15": -0.4807,
        "v16": 1.4386,
        "v17": 0.0683,
        "v18": -1.2506,
        "v19": -1.3116,
        "v20": -0.1564,
        "v21": 0.5852,
        "v22": 1.0929,
        "v23": 0.3316,
        "v24": -0.1778,
        "v25": -0.9729,
        "v26": -0.4784,
        "v27": 0.2226,
        "v28": 1.0997
    }]'
    ```