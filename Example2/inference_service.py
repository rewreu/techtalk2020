from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import *
from pyspark.ml.linalg import DenseVector
from pyspark.ml.classification import RandomForestClassificationModel
from pyspark.ml.feature import IndexToString, StringIndexer, VectorIndexer,HashingTF, Tokenizer
from pyspark.ml import PipelineModel
from sys import platform
import findspark
import os
import json

spark_version = os.environ["SPARK_VERSION"]
findspark.init("/usr/local/spark-{}-bin-hadoop2.7".format(spark_version))

# Build the SparkSession
spark = SparkSession.builder \
    .master("local") \
    .appName("rf Model") \
    .config("spark.executor.memory", "1gb") \
    .getOrCreate()
sc = spark.sparkContext
pipcls = PipelineModel.load("./model_cls")

# Handles json as input
# d = {"v1":0.876, "v2":0.892, ..., "v28":0.919}
def create_item(d):
   a = [json.dumps(d)]
   jsonRDD = sc.parallelize(a)
   df = spark.read.json(jsonRDD)
   df = df.select("v1", "v2", "v3", "v4", "v5", "v6",
                  "v7", "v8", "v9", "v10", "v11", "v12", "v13", "v14",
                  "v15", "v16", "v17", "v18", "v19", "v20", "v21", "v22",
                  "v23", "v24", "v25", "v26", "v27", "v28")
   input_data = df.rdd.map(lambda x: ("_placeholder", DenseVector(x[0:])))
   df = spark.createDataFrame(input_data, ["_placeholder", "features"]).select("features")
   a = pipcls.stages[1].transform(df)
   predicted = pipcls.stages[2].transform(a)
   tdc = predicted.select("prediction").toJSON().collect()
   dc = eval(tdc[0])
   # return a json {"prediction": 0}
   return {'prediction':int(dc['prediction'])}


