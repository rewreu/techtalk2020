# How to use

1. Download the spark tar file, save in the same folder as dockerfile
2. Change SPARK_VERSION in the docker file according to step 1. 
3. Run: 
   `
    build . --tag dockerimagename:tag
   `