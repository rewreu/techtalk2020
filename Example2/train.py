# the training data is from:
# https://www.kaggle.com/mlg-ulb/creditcardfraud
import pyspark
from pyspark.sql import SQLContext
from pyspark import SparkContext
from pyspark.sql import Row, functions
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.ml.feature import IndexToString, StringIndexer, VectorIndexer, HashingTF, Tokenizer
from pyspark.ml.classification import RandomForestClassifier
from pyspark.ml import Pipeline
from pyspark.ml.regression import RandomForestRegressor
from pyspark.ml.feature import VectorIndexer
from pyspark.ml.evaluation import RegressionEvaluator
import findspark
import os
from sys import platform
from pyspark.sql import SparkSession
from pyspark.ml.linalg import DenseVector


os.environ["SPARK_LOCAL_IP"] = "127.0.0.1"
if platform == "darwin":
   findspark.init("/usr/local/Cellar/apache-spark/2.4.4/libexec")
elif platform == "linux2":
   findspark.init("/usr/local/spark-2.4.4-bin-hadoop2.7")

# Build the SparkSession
spark = SparkSession.builder \
    .master("local") \
    .appName("Linear Regression Model") \
    .config("spark.executor.memory", "1gb") \
    .getOrCreate()

sc = spark.sparkContext

def f(x):
    rel = {}
    rel['features'] = DenseVector([float(x[1]), float(x[2]), float(x[3]), float(x[4]), float(x[5]), float(x[6]), float(x[7]), float(x[8]), float(x[9]), float(x[10]), float(x[11]), float(x[12]), float(x[13]), float(
        x[14]), float(x[15]), float(x[16]), float(x[17]), float(x[18]), float(x[19]), float(x[20]), float(x[21]), float(x[22]), float(x[23]), float(x[24]), float(x[25]), float(x[26]), float(x[27]), float(x[28])])
    rel['label'] = str(x[-1])
    rel['amtlabel'] = float(x[-2])
    return rel


df = sc.textFile("./creditcard_r.csv").map(
    lambda line: line.split(',')).map(lambda p: Row(**f(p))).toDF()

labelIndexer = StringIndexer().setInputCol(
    "label").setOutputCol("indexedLabel").fit(df)

featureIndexer = VectorIndexer().setInputCol("features").setOutputCol(
    "indexedFeatures").setMaxCategories(2).fit(df)

labelConverter = IndexToString().setInputCol("prediction").setOutputCol(
    "predictedLabel").setLabels(labelIndexer.labels)
trainingData, testData = df.randomSplit([0.7, 0.3])


rf = RandomForestClassifier(
    labelCol="indexedLabel", featuresCol="indexedFeatures", numTrees=10)

pipelinedClassifier = Pipeline().setStages(
    [labelIndexer, featureIndexer, rf, labelConverter])


# train the model
modelClassifier = pipelinedClassifier.fit(trainingData)

# predict the result
predictionsClassifier = modelClassifier.transform(testData)

# look the result
predictionsClassifier.select("predictedLabel", "label", "features").show(5)
# Select (prediction, true label) and compute test error
evaluator = MulticlassClassificationEvaluator(
    labelCol="indexedLabel", predictionCol="prediction")
accuracy = evaluator.evaluate(predictionsClassifier)
print("Test Error = %g" % (1.0 - accuracy))

rfModel = modelClassifier.stages[2]
print(rfModel)  # summary only

# save model to persist
modelClassifier.save("./model_cls")

sc.stop()
