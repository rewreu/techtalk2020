import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestRegressor
import pickle


INPUT_DIM = 33
col_name = ["c"+str(x) for x in range(1,INPUT_DIM+1)]

darr = np.random.randint(0,20,(100000,INPUT_DIM))
df = pd.DataFrame(darr, columns=col_name)

df["sum"] = df.sum(axis=1)

regr = RandomForestRegressor(max_depth=20, random_state=0)
regr.fit(df[col_name], df["sum"])


regr.predict(df[col_name][2:5])

s= pickle.dump(regr,open("./kml/model.pkl", 'wb'))


import json
ll=[]
for col in col_name:
    ll.append({"col_name": col, "col_type": "int"})
print(json.dumps(ll))